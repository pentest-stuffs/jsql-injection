# jSQL Injection 0.81

jSQL Injection is a lightweight application used to find database information
from a distant server.

jSQL Injection is free, open source and cross-platform.

## Features
* Automatic injection of 23 kinds of databases: Access, CockroachDB, CUBRID,
DB2, Derby, Firebird, H2, Hana, HSQLDB, Informix, Ingres, MaxDB, Mckoi, MySQL,
Neo4j, NuoDB, Oracle, PostgreSQL, SQLite, SQL Server, Sybase, Teradata and Vertica
* Multiple injection strategies: Normal, Error, Blind and Time
* Multiple injection structures: Standard, Zipped, Dump In One Shot
* SQL Engine to study and optimize SQL expressions
* Injection of multiple targets
* Search for administration pages
* Creation and vizualisation of Web shell and SQL shell
* Read and write files on host using injection
* Bruteforce of password's hash
* Encode and decode a string

Homepage: https://github.com/ron190/jsql-injection

jSQL Injection was tested in this linux distributions:
* Debian Stretch
* Linux Mint

## INSTALLATION

### Dependencies:
jSQL Injection is a java tool, which need for a correct running a few another
packages.

If you will install this by the `dpkg -i`, then make sure, that you have
installed all the depending packages:

```
apt-get install default-jre openjdk-8-jre
```

### Install from source:
Unpack the source package and run command (like root):

```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
dpkg -i jsql-injection*.deb
```
